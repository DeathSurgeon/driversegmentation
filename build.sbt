name := "goJek"

version := "1.0"

scalaVersion := "2.11.0"


ivyScala := ivyScala.value map {
  _.copy(overrideScalaVersion = true)
}

libraryDependencies += "org.apache.spark" % "spark-sql_2.11" % "2.2.0" % "provided"

libraryDependencies += "org.apache.spark" % "spark-core_2.11" % "2.2.0" % "provided"

libraryDependencies ++= {
  Seq(

    "joda-time" % "joda-time" % "2.9.4"

  )
}