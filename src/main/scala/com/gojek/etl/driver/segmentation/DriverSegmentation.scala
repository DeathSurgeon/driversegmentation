package com.gojek.etl.driver.segmentation

import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.joda.time.{DateTime, DateTimeZone}
import org.joda.time.format.DateTimeFormat
import org.apache.spark.sql.functions._

import scala.collection.mutable
import scala.collection.mutable.ListBuffer


case class DriverSegmentation(inputBucket: String, outputBucket: String, driverCategory: String,
                             executionDate: String, bucketString : String) extends Segmentation{


  val sparkSession = SparkSession.builder.
    appName("Segmentation")
    .getOrCreate()

  import sparkSession.implicits._

  val bucketMap = getBucketMap(bucketString)

  def readFile(path: String): DataFrame = {
    sparkSession.sqlContext.read.parquet(path)
  }

  def filterVehicleType(df : DataFrame): DataFrame = {

    df.columns.contains("vehicle_type") match {
      case true =>
        df.filter(s"vehicle_type = '${driverCategory}'")
      case false =>
        //Throw an exception
        null
    }
  }

  def getInputPath(): String = {

    val dt = DateTime.parse(executionDate, DateTimeFormat.forPattern("yyyy-MM-dd")).withZone(DateTimeZone.UTC)

    "gs://" + inputBucket + "/dt=" + dt.minusDays(1).toString("yyyy-MM-dd") + "/*/*"

  }

  def bucketize_driver(total_dist : Double, duty_dist : Double) : Int = {
    /*var bucket = 0
    val ratio = (duty_dist/total_dist)
    if (ratio <= 0.5) {
      bucket = 1 //roaming around, looking for hail
    } else if (ratio > 0.5 && ratio <= 0.8) {
      bucket = 2 //roams somewhat and accepts ride and goes on trip
    } else {
      bucket = 3 //stationary at hotspot places like malls, etc. and moves on rides only
    }
    bucket*/
    val ratio = (duty_dist/total_dist)

    val a = bucketMap.keys.find(f => {
      (f.lowerBound < ratio) && (ratio <= f.upperBound)
    })

    a.isDefined match {
      case true =>
        bucketMap(a.get)
      case false => -1
    }

  }

  def groupByDriverId(df : DataFrame): DataFrame = {
    df.groupBy("driver_id").agg(collect_list(struct("driver_location.latitude",
            "driver_location.longitude", "driver_status",
            "event_timestamp.seconds"))
        .as("locAttr"))
  }

  def bucketize(df : DataFrame): DataFrame = {
    val status_array = Seq("OTW_PICKUP", "OTW_DROPOFF")
    val total_distance_threshold = 333.0  //distance in km
    val results = df.map {
      row => {
        var driver_duty_distance = 0.0
        var driver_total_distance = 0.0
        val driver_id = row.getString(0)
        val z = row.getAs[mutable.WrappedArray[Row]](1)
        var listBuffer = new ListBuffer[LocAttr]()
        z.map {
          case Row(lat : Double, lon : Double, status : String, sec : Long) => {
            listBuffer += LocAttr(lat,lon,status,sec)
          }
          case _ =>
        }
        val list = listBuffer.toList
        val sortedList = list.sortWith(_.sec < _.sec)
        for (i <- 1 until sortedList.length) {
          driver_total_distance += haversine_distance(sortedList(i-1), sortedList(i))
          if ((status_array contains sortedList(i-1).status) && ((status_array contains sortedList(i).status))) driver_duty_distance +=  haversine_distance(sortedList(i-1), sortedList(i))
        }
        if (driver_total_distance < 10.0) {
          ("Stationary", driver_id, driver_total_distance, driver_duty_distance, 0)
        } else if (driver_total_distance >= 10.0 && driver_total_distance <= total_distance_threshold) {
          ("Benchmark", driver_id, driver_total_distance, driver_duty_distance, bucketize_driver(driver_total_distance, driver_duty_distance))
        } else {
          ("Outlier", driver_id, driver_total_distance, driver_duty_distance, 0)
        }
      }
    }.toDF("category", "driver_id", "driver_total_distance",
      "driver_duty_distance", "bucket")

    results.createTempView("table")

    sparkSession.sqlContext.sql("select bucket, count(*) from table group by bucket").show

    results
  }

  def getOutputPath() : String = {
    val dt = DateTime.parse(executionDate, DateTimeFormat.forPattern("yyyy-MM-dd")).withZone(DateTimeZone.UTC)
    "gs://" + outputBucket + "/dt=" + dt.minusDays(1).toString("yyyy-MM-dd") + "/"
  }

}

case class result(category : String, driver_id : String, driver_total_distance: Double,
                  driver_duty_distance: Double, bucket : Int)

case class LocAttr(var lat: Double, var lon : Double, var status : String, var sec : Long)