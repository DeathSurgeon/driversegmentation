package com.gojek.etl.driver.segmentation

object Main extends App {

  val executionDate = System.getProperty("executionDate")
  val inputBucket = System.getProperty("inputBucket")
  val outputBucket = System.getProperty("outputBucket")
  val driverCategory = System.getProperty("driverCategory")
  val bucketRanges = System.getProperty("bucketRanges")

  val driverSegmentation = DriverSegmentation(inputBucket, outputBucket,
                                               driverCategory, executionDate, bucketRanges)

  println(driverSegmentation)

  val path = driverSegmentation.getInputPath()
  println("inputPath : " + path)

  val df = driverSegmentation.readFile(path)
  println("total number of records in the cold storage : " + df.count())

  val filteredDf = driverSegmentation.filterVehicleType(df)
  println("number of records in the filtered dataframe : " + filteredDf.count())

  val groupedDf = driverSegmentation.groupByDriverId(filteredDf)
  println("number of unique driver ids : " + groupedDf.count())

  val result = driverSegmentation.bucketize(groupedDf)

  result.write.option("header", "true").mode("overwrite").csv(driverSegmentation.getOutputPath())
  println("writing to the output bucket is done :) ")

  driverSegmentation.sparkSession.stop()
  System.exit(0)

}