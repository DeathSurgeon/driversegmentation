package com.gojek.etl.driver.segmentation

import scala.math._


trait Segmentation {

  val R = 6372.8  //radius in km

  def haversine_distance(a : LocAttr, b : LocAttr) : Double = {
    val dLat=(b.lat - a.lat).toRadians
    val dLon=(b.lon - a.lon).toRadians
    val dst = pow(sin(dLat/2),2) + pow(sin(dLon/2),2) * cos(a.lat.toRadians) * cos(b.lat.toRadians)
    val c = 2 * asin(sqrt(dst))
    R * c
  }

  def getInputPath(): String

  def bucketize_driver(total_dist : Double, duty_dist : Double) : Int

  def getOutputPath(): String

  def getBucketMap(s : String): Map[Range, Int] = {
    val bucketSeq = s.split(",").map(_.trim.toDouble)

    val bucketSeq2 = Seq(0.0) ++ bucketSeq

    bucketSeq2.zip(bucketSeq ++ Seq(1.0))
      .zipWithIndex
        .drop(1)
        .map(x => {
      Range(x._1._1, x._1._2) -> x._2
    }).toMap

  }
}

case class Range(lowerBound: Double, upperBound : Double)